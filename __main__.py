from app import server
import sys
import os

server_password = sys.argv[1]
basedir = os.path.abspath(os.path.dirname(__file__))
cafile = os.path.join(basedir, 'app/certs/localwifi.pem')
mtserver = server.Server('', 8911, 5, server_password, cafile)
mtserver.run()