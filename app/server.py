import socket
import datetime
import threading
import sys
import json
import traceback
import bcrypt
import os
import ssl
from models.models import Client, ChatRoom, Base, User, Room
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker

basedir = os.path.abspath(os.path.dirname(__file__))
engine = create_engine('sqlite:////' + os.path.join(basedir, 'app.db'))
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)

class Server:
    def __init__(self, host, port, mouths, pwd, cafile):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = host
        self.port = port
        self.server_pw = self.hash_password(pwd)
        self.sock.bind((host, port))
        self.sock.listen(mouths)
        self.client_list = []
        self.rooms = self.init_rooms()
        self.voip_rooms = self.init_voip_rooms()
        self.auth_lock = threading.Lock()
        self.create_dbroom_lock = threading.Lock()
        self.purpose = ssl.Purpose.CLIENT_AUTH
        self.context = ssl.create_default_context(self.purpose, cafile=cafile)
        self.context.load_cert_chain(cafile)

    def run(self):
        loop = 0
        while True:
            try:
                if loop < 1:
                    print("Waiting for connections..")
                connection, address = self.sock.accept()
                ssl_socket = self.context.wrap_socket(connection, server_side=True)
                cl = Client(ssl_socket, address)
                authenticate_user_thread = threading.Thread(target=self.receive_credentials, args=(cl,))
                authenticate_user_thread.start()
            except Exception as e:
                tb = traceback.format_exc()
                print("Exception in main thread!")
                print(str(e) + " " + str(tb))
            loop += 1

    def init_rooms(self):
        session = Session()
        try:
            roomlist = []
            roomlist = self.fetch_rooms_from_db(session)
            session.commit()
        except Exception as e:
            print(e)
            session.rollback()
        finally:
            session.close()
            return roomlist

    def init_voip_rooms(self):
        voip_room_list = []
        for room in self.rooms:
            voip_room_list.append(ChatRoom(room.get_id(), room.get_room_name()))
        return voip_room_list

    def receive_credentials(self, client):
        data = bytearray()
        msg = ''
        while not msg:
            recvd = client.get_connection().recv(2048)
            if not recvd:
                raise Exception()
            data = data + recvd
            if b'[\x00\x00\x00\x00]' in recvd:
                msg = data
        msg_null_stripped = msg.replace(b'[\x00\x00\x00\x00]', b'')
        msg_stripped = msg_null_stripped.replace(b'[\x0B\x0B\x0B\x0B]', b'')
        name, timestamp, message, extra = self.unwrap_data(msg_stripped.decode('utf-8'))
        self.serve_client(name, timestamp, message, extra, client)

    def recv_messages(self, client):
        try:
            while True:
                data = bytearray()
                msg = ''
                while not msg:
                    recvd = client.get_connection().recv(1024)
                    if not recvd:
                        raise Exception()
                    data = data + recvd
                    if b'[\x00\x00\x00\x00]' in recvd:
                        msg = data
                if b'[\x0B\x0B\x0B\x0B]' in msg:
                    msg_null_stripped = msg.replace(b'[\x00\x00\x00\x00]', b'')
                    msg_stripped = msg_null_stripped.replace(b'[\x0B\x0B\x0B\x0B]', b'')
                    name, timestamp, message, extra = self.unwrap_data(msg_stripped.decode('utf-8'))
                    serve_client_thread = threading.Thread(target=self.serve_client, args=(name, timestamp, message, extra, client), daemon=False)
                    serve_client_thread.start()
                else:
                    client.get_message_queue().put((msg, None))
        except Exception:
            tb = traceback.format_exc()
            print(str(tb))
            print(recvd)
            self.handle_disconnect(client)

    def send_messages(self, client):
        while True:
            message = client.get_message_queue().get()
            if message[0] is None:
                break
            elif b'[\x1A\x1A\x1A\x1A]' in message[0]:
                if client.get_voip_room() is not None:
                    voip_client_list = client.get_voip_room().get_client_list()
                    for cli in voip_client_list:
                        if cli is not client:
                            cli.get_connection().sendall(message[0])
            elif b'[\x0B\x0B\x0B\x0B]'in message[0]:
                client.get_connection().sendall(message[0])
            elif message[1] is None:
                room_client_list = client.get_current_room().get_client_list()
                for cli in room_client_list:
                    if cli is not client:
                        cli.get_connection().sendall(message[0])
            else:
                room_client_list = message[1].get_client_list()
                for cli in room_client_list:
                    if cli is not client:
                        cli.get_connection().sendall(message[0])
            client.get_message_queue().task_done()


    def create_message_dict(self, name, message, *extra):
        timestamp = self.get_datetime()
        data_dict = {
            "name": name,
            "timestamp": timestamp,
            "message": message,
            "extra": extra
        }
        return data_dict

    def get_datetime(self):
        dt = datetime.datetime.now()
        dt_string = dt.strftime("%d.%m.%y, %H:%M")
        return dt_string

    def handle_disconnect(self, client):
        print("{}:{} disconnected.".format(
            client.get_address()[0], client.get_address()[1]))
        room = client.get_current_room()
        room.remove_client(client)
        self.notify_chatroom_disconnect(client, room)
        self.client_list.remove(client)
        client.get_message_queue().put((None,None))
        client.get_connection().close()

    def notify_chatroom_join(self, client, *args):
        joinmessage = "{} joined.".format(client.get_name())
        data_dict = self.create_message_dict("server", joinmessage)
        if len(args) == 0:
            msg = self.prep_data(data_dict, False)
            client.get_message_queue().put((msg, None))
        else:
            msg = self.prep_data(data_dict, False)
            client.get_message_queue().put((msg, args[0]))

    def notify_chatroom_disconnect(self, client, *args):
        farawell = "{} left.".format(client.get_name())
        data_dict = self.create_message_dict("server", farawell)
        if len(args) == 0:
            msg = self.prep_data(data_dict, False)
            client.get_message_queue().put((msg, None))
        else:
            msg = self.prep_data(data_dict, False)
            client.get_message_queue().put((msg, args[0]))

    def prep_data(self, data_dict, *args):
        if len(args) == 0:
            data_str = json.dumps(data_dict, ensure_ascii=False)
            data_str = data_str + '[\x00\x00\x00\x00]'
            return data_str.encode('utf-8')
        elif args[0] == True:
            data_str = json.dumps(data_dict, ensure_ascii=False)
            data_str = data_str + '[\x0B\x0B\x0B\x0B]' + '[\x00\x00\x00\x00]'
            return data_str.encode('utf-8')
        elif args[0] == False:
            data_str = json.dumps(data_dict, ensure_ascii=False)
            data_str = data_str + '[\x1B\x1B\x1B\x1B]' + '[\x00\x00\x00\x00]'
            return data_str.encode('utf-8')

    def unwrap_data(self, message):
        data_dictionary = json.loads(message)
        senders_name = data_dictionary.get("name")
        timestamp = data_dictionary.get("timestamp")
        senders_message = data_dictionary.get("message")
        senders_extra = data_dictionary.get("extra")
        return (senders_name, timestamp, senders_message, senders_extra)

    def get_room_by_name(self, roomname):
        room = None
        for r in self.rooms:
            if r.get_room_name() == roomname:
                room = r
                break
        return room

    def serve_client(self, name, timestamp, message, extra, client):
        if message == 'getrooms':
            self.get_rooms(client)
        elif message == 'setname':
            self.set_name(client, name)
        elif message == 'switchroom':
            self.switch_room(client, extra)
        elif message == 'createroom':
            self.create_room_and_switch(client, extra)
        elif message == 'joinvoip':
            self.join_voip_room(client)
        elif message == 'leavevoip':
            self.leave_voip_room(client)
        elif message == 'authenticate':
            if self.authenticate(name, extra, client):
                msg = "Logged in succesfully."
                data_dict = self.create_message_dict("server", msg, "True")
                message = self.prep_data(data_dict, True)
                client.get_connection().sendall(message)
                self.client_list.append(client)
                self.rooms[0].add_client(client)
                client.set_current_room(self.rooms[0])
                print("Connection from {} on port {}".format(client.get_address()[0], client.get_address()[1]))
                recv_messages_thread = threading.Thread(
                    target=self.recv_messages, args=(client,), daemon=True)
                send_messages_thread = threading.Thread(
                    target=self.send_messages, args=(client,), daemon=True)
                recv_messages_thread.start()
                send_messages_thread.start()
            else:
                msg = "Password incorrect."
                data_dict = self.create_message_dict("server", msg, "False")
                message = self.prep_data(data_dict, True)
                client.get_connection().sendall(message)
                client.get_connection().close()
                del client

    def authenticate(self, name, extra, client):
        pw_recvd = extra[0]
        if self.check_password(pw_recvd):
            self.auth_lock.acquire()
            session = Session()
            try:
                self.do_after_pw_success(name, extra, session)
                session.commit()
            except Exception as e:
                tb = traceback.format_exc()
                print("Errormsg: " + str(e) + ", " + str(tb))
                session.rollback()
            finally:
                session.close()
            self.auth_lock.release()
            return True
        else:
            return False

    def do_after_pw_success(self, name, extra, session):
        users = session.query(func.count('*')).select_from(User).scalar()
        print("Users count = " + str(users))
        print("UID received: " + extra[1])
        user = session.query(User).filter_by(uid=extra[1]).first()
        if user is None:
            print("User not found, creating one...")
            user = User(uid=extra[1], username=name)
            session.add(user)
        else:
            print(str(user.id) + " " + user.username + " " + str(user.uid))
            print("User exists. Nice.")

    def fetch_rooms_from_db(self, session):
        roomlist = []
        rooms = session.query(Room).all()
        if len(rooms) == 0:
            ml_id = 1
            roomname = "mainlobby"
            mainlobby_r = Room(name=roomname)
            mainlobby_cr = ChatRoom(ml_id, roomname)
            session.add(mainlobby_r)
            roomlist.append(mainlobby_cr)
            print("Mainlobby created.")
        else:
            for room in rooms:
                cr_id = room.id
                cr_name = room.name
                cr = ChatRoom(cr_id, cr_name)
                roomlist.append(cr)
        return roomlist

    def join_voip_room(self, client):
        chat_room = client.get_current_room()
        for voip_room in self.voip_rooms:
            if chat_room.get_id() == voip_room.get_id():
                voip_room.add_client(client)
                client.set_voip_room(voip_room)
                break
        print("Clients voip room is: {}".format(client.get_voip_room().get_room_name()))
        v_msg = "joinvoip_response"
        data_dict = self.create_message_dict("server", v_msg, True)
        msg = self.prep_data(data_dict, True)
        client.get_message_queue().put((msg, None))

    def leave_voip_room(self, client):
        voip_room = client.get_voip_room()
        voip_room.remove_client(client)
        client.clear_voip_room()
        v_msg = "leavevoip_response"
        data_dict = self.create_message_dict("server", v_msg, True)
        msg = self.prep_data(data_dict, True)
        client.get_message_queue().put((msg, None))

    def switch_room(self, client, extra):
        current_room = client.get_current_room()
        destination = extra[0]
        if current_room.get_room_name() == destination:
            notify_msg = "You are currently in that room."
            data_dict = self.create_message_dict("server", notify_msg)
            msg = self.prep_data(data_dict, True)
            client.get_message_queue().put((msg, None))
        else:
            destination_room = self.get_room_by_name(destination)
            self.do_room_switch(client, current_room, destination_room)

    def do_room_switch(self, client, current_room, destination_room):
        #TODO: Add clientlist to extra.
        if destination_room is not None:
            self.notify_chatroom_disconnect(client, current_room)
            current_room.remove_client(client)
            destination_room.add_client(client)
            client.set_current_room(destination_room)
            self.notify_chatroom_join(client, destination_room)
            msg = "roomswitch_response"
            clientnamelist = []
            for cl in client.get_current_room().get_client_list():
                if client.get_name() is not cl.get_name():
                    clientnamelist.append(cl.get_name())
            data_dict = self.create_message_dict("server", msg, True, client.get_current_room().get_room_name(), clientnamelist)
            message = self.prep_data(data_dict,True)
            client.get_message_queue().put((message, None))
        else:
            msg = "roomswitch_response"
            data_dict = self.create_message_dict("server", msg, False)
            message = self.prep_data(data_dict, True)
            client.get_message_queue().put((message, None))

    def create_room_and_switch(self, client, extra):
        newroom_id = len(self.rooms) + 1
        newroom = ChatRoom(newroom_id, extra[0])
        self.rooms.append(newroom)
        self.create_dbroom_lock.acquire()
        session = Session()
        try:
            self.create_room_to_db(extra, session)
            session.commit()
        except Exception as e:
            print(str(e))
            session.rollback()
        finally:
            session.close()
        self.create_dbroom_lock.release()
        msg = "Room created succesfully."
        data_dict = self.create_message_dict("server", msg)
        message = self.prep_data(data_dict, True)
        client.get_message_queue().put((message, None))
        current_room = client.get_current_room()
        self.do_room_switch(client, current_room, newroom)

    def create_room_to_db(self, extra, session):
        room = Room(name=extra[0])
        session.add(room)

    def get_rooms(self, client):
        #TODO: Add clientlist to extra.
        roomnamelist = []
        for room in self.rooms:
            roomnamelist.append(room.get_room_name())
        clientnamelist = []
        for cl in client.get_current_room().get_client_list():
            if client.get_name() is not cl.get_name():
                clientnamelist.append(cl.get_name())
        data_dict = self.create_message_dict("server", "getrooms_response", client.get_current_room().get_room_name(), roomnamelist, clientnamelist)
        msg = self.prep_data(data_dict, True)
        client.get_message_queue().put((msg, None))

    def set_name(self, client, name):
        if client.get_name() is None:
            client.set_name(name)
            self.notify_chatroom_join(client)
        else:
            client.set_name(name)
            name_message = "Your name was set to {}".format(name)
            data_dict = self.create_message_dict("server", name_message)
            name_set_msg = self.prep_data(data_dict, True)
            client.get_message_queue().put((name_set_msg, None))

    def hash_password(self, password):
        hashed = bcrypt.hashpw(bytes(password, 'utf-8'), bcrypt.gensalt())
        return hashed

    def check_password(self, password):
        if bcrypt.checkpw(bytes(password,'utf-8'), self.server_pw):
            return True
        else:
            return False