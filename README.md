# Speech Bubble Server

This project was made as a part of my thesis. Client project can be found here: https://gitlab.com/Talkhunat/speech-bubble-client
If you have any sort of problems with running the app, feel free to contact me. 
You should also note that this is a work in progress and far from perfect, but it taught me many things and I'm looking forward to make it better!

## What you need
    1. Python (Tested with 3.6, but all versions above 3 should work.)
    2. Pip
    3. OpenSSL (I recommend using Windows subsystem for Linux and installing Ubuntu for example)
    4. Works on Windows/Ubuntu/OS X (Tested with Ubuntu and Windows)

## Setting up Speech Bubble server
    1. Install requirements.txt with command 'pip install -r requirements.txt'
        --> Note: In case you get an error from 'pkg-resources==0.0.0', just delete it from requirements.txt and save.
        --> Note2: I recommend using virtualenvs when installing packages so that you don't get version conflicts with your systems global Python.
        --> Note3: If some package is missing from the requirements.txt, just install it with 'pip install packagenamehere'.
    2. Generate a certificate and private key with OpenSSL CLI
        --> You can use this command for example: 'openssl req -x509 -newkey rsa:4096 -nodes -keyout privatekey.key -out certificate.crt -days 365'
        --> Concatenate these files with command 'cat certificate.crt privatekey.key > cert.pem' to .pem format and place it to 'app/certs/'-folder.
            --> Move this certificate.crt to client application's folder 'certs/' in the project's root directory. 
        --> *Make sure* that main-module in the root of the project has correct name for the certificate and that you gave correct host ip-address for the cert.
    3. Portnumber can be changed in the main-module (Parameter in server.Server-constructor)

## Running the server
    1. Using command prompt, cd out of the project folder. Call 'python path-to-project-directory server-password' 
        --> For example: 'python /home/user/speech-bubble-server/ supersecret_password'
        --> With this password, users will be able to connect to the server.
    2. If the program prints 'Waiting for connections...', you're all set! 