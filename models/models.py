import queue
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Sequence

Base = declarative_base()

#TODO: Add roles and bans to db.
#TODO: Create table for username aliases
#TODO: Create table for chatrooms
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, Sequence('user_id_seq'), primary_key = True)
    uid = Column(Integer)
    username = Column(String)

class Room(Base):
    __tablename__ = 'rooms'
    id = Column(Integer, Sequence('room_id_seq'), primary_key=True)
    name = Column(String)

class Client:
    def __init__(self, conn, addr):
        self.name = None
        self.connection = conn
        self.address = addr
        self.room = None
        self.voip_room = None
        self.message_queue = queue.Queue()

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def get_address(self):
        return self.address

    def get_connection(self):
        return self.connection

    def get_message_queue(self):
        return self.message_queue

    def get_current_room(self):
        return self.room

    def set_current_room(self, room):
        self.room = room

    def get_voip_room(self):
        return self.voip_room

    def set_voip_room(self, room):
        self.voip_room = room

    def clear_voip_room(self):
        self.voip_room=None

class ChatRoom:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.client_list = []

    def get_id(self):
        return self.id

    def get_room_name(self):
        return self.name

    def add_client(self, client):
        self.client_list.append(client)

    def remove_client(self, client):
        self.client_list.remove(client)

    def get_client_list(self):
        return self.client_list